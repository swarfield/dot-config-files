# .bashrc
#tty >/dev/null && command -v zsh >/dev/null && exec zsh

# Source global definitions
if [ -f /etc/bashrc ]; then
       . /etc/bashrc
fi
export GPG_TTY=$(tty)

# Enable Powerline
#if [ -f `which powerline-daemon` ]; then
#  powerline-daemon -q
#  POWERLINE_BASH_CONTINUATION=1
#  POWERLINE_BASH_SELECT=1
#  . /usr/share/powerline/bash/powerline.sh
#fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias isengard="ssh warfield@isengard.mines.edu"
#alias desktopComp="ssh warfield@138.67.75.234"

export GOPATH=$HOME/go
alias zdrive="smbclient -U warfield -W adit //hornet/users"
alias imagine="ssh warfield@pvm-jumpbox.mines.edu"
alias fsIsengard="$HOME/Code/dot-config-files/fsIsen.sh"
alias encom='ssh swarfield@encom.warfiel.dev'
alias parallel="ssh warfield@eecs-hpc-1.mines.edu"
alias fsParallel="sshfs warfield@eecs-hpc-1.mines.edu:/home/warfield $HOME/parallel/"

alias got="git"
alias pull="git pull"
#alias vis="visplay --config ~/Code/visplay/config.yaml"
alias printIsen="ssh isengard.mines.edu lpr -P bb136-printer -o coallate=true"
alias printIsenDouble="ssh isengard.mines.edu lpr -P bb136-printer -o coallate=true -o Duplex=DuplexNoTumble"
alias Clue="cd ~/Code/CSCI306/Clue"
alias pi="ssh -Xp 6989 pi@73.153.135.25"
alias ls="ls --color"
alias i3config="nano ~/.config/i3/config"
alias factorioServer="ssh factorio@157.230.150.145"
alias flowers='psql postgresql://warfield@flowers.mines.edu/csci403'
alias fsEncom="sshfs swarfield@encom.warfiel.dev:/home/swarfield $HOME/encom"
export PATH=$PATH:~/.local/bin/

fortune
echo

