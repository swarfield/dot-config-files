#!/usr/bin/env bash

ln init.vim  ~/.config/nvim/init.vim # Links the neo-vim Config
ln .bashrc ~/.bashrc
ln i3/config ~/.config/i3/config
ln i3/status.toml ~/.config/i3/status.toml
ln -s backgrounds ~/backgrounds

cd i3status-rust && cargo build --release

ln target/release/i3status-rs ~/.config/i3/ 
